{
    /*
    const codeLookup = ['0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-']; // this is the same list as base 64, but we're going to use a bit differently
    //  0 = N + N + N
    //  1 = N + N + S
    //  2 = N + N + E
    //  3 = N + N + W
    //  4 = N + S + N
    //  5 = N + S + S
    //  6 = N + S + E
    //  7 = N + S + W
    //  8 = N + E + N
    //  9 = N + E + S
    //  A = N + E + E
    //  B = N + E + W
    //  C = N + W + N
    //  D = N + W + S
    //  E = N + W + E
    //  F = N + W + W
    //  G = S + N + N
    //  H = S + N + S
    //  I = S + N + E
    //  J = S + N + W
    //  K = S + S + N
    //  L = S + S + S
    //  M = S + S + E
    //  N = S + S + W
    //  O = S + E + N
    //  P = S + E + S
    //  Q = S + E + E
    //  R = S + E + W
    //  S = S + W + N
    //  T = S + W + S
    //  U = S + W + E
    //  V = S + W + W
    //  W = E + N + N
    //  x = E + N + S
    //  y = E + N + E
    //  Z = E + N + W
    //  a = E + S + N
    //  b = E + S + S
    //  c = E + S + E
    //  d = E + S + W
    //  e = E + E + N
    //  f = E + E + S
    //  g = E + E + E
    //  h = E + E + W
    //  i = E + W + N
    //  j = E + W + S
    //  k = E + W + E
    //  l = E + W + W
    //  m = W + N + N
    //  n = W + N + S
    //  o = W + N + E
    //  p = W + N + W
    //  q = W + S + N
    //  r = W + S + S
    //  s = W + S + E
    //  t = W + S + W
    //  u = W + E + N
    //  v = W + E + S
    //  w = W + E + E
    //  x = W + E + W
    //  y = W + W + N
    //  z = W + W + S
    //  - = W + W + E
    //  + = W + W + W
    // 
    //  instead of decode 4 chars to 3 bytes, we use these characters directly.
    //  For each cell in the grid we only need to know the orientation (NESW), which has 4 possible values, sotreable in 2 bits
    //  thus we can assign 2 bits as follows:
    //  bit 0: N/S or E/W
    //  bit 1: N/E or S/W
    //  the 64 values above can be stored in 6 bits, which means we can have 3 pairs of 2 bits
    //  Thus we can store the whole grid fairly efficiently using only printable characters.
    //  if there are unused bits on the end we can put 1 or 2 =, similar to base64 to indicate if there are 2 or 4 unused bits that should be ignored
    //  One snag - during processing we need a 5th possible value to represent an empty cell.
    //  if we try to use the binary approach here we'll need a whole extra bit per cell, which means we can no longer fit 2 cells into a single char.
    //  if we drop it to just 2 cells per char then there are only 5x5=25 possible values for each cell. Which is quite wasteful to represent with ascii chars.
    //  However, we only need these special values during processing, so we could decode the above presentation into a single char per cell representation instead. Thus giving NESW#
    //  thus an empty 2x2 grid is ####
    */
}
// let grid = [...'####'];
// let gridSize = 2;
// const codeReverseLookup = {};
// for (const idx in codeLookup) {
//     codeReverseLookup[codeLookup[idx]] = idx;
// }
const orangeReplacements = ['NnSs', 'WEwe'];

const divOutput = document.getElementById('divOutput');
//divOutput.classList.add('Output');
// document.body.append(divOutput);
let grid = [...'####'];
let gridSize = 2;
processOrangeBlocks(grid, gridSize);
drawGrid(gridSize, grid, divOutput);

function* expandSteps() {
    // yield 
    grid = expandGrid(grid, gridSize);
    gridSize += 2;
    // drawGrid(gridSize, grid, divOutput);
    // yield 
    removeFacing(grid, gridSize);
    // drawGrid(gridSize, grid, divOutput);
    // yield 
    grid = shuffle(grid, gridSize);
    // drawGrid(gridSize, grid, divOutput);
    // yield 
    processOrangeBlocks(grid, gridSize);
    drawGrid(gridSize, grid, divOutput);
    yield;
}
function quickExpand() {
    processOrangeBlocks(grid, gridSize);
    grid = expandGrid(grid, gridSize);
    gridSize += 2;
    removeFacing(grid, gridSize);
    grid = shuffle(grid, gridSize);
    processOrangeBlocks(grid, gridSize);
}


function drawGrid(rowCount, grid, dest) {
    dest.innerHTML = '';
    let idx = 0;
    let rowSize = 2;
    for (let rowNum = 0; rowNum < rowCount / 2; rowNum++) {
        let divRow = document.createElement('div');
        for (let cell = 0; cell < rowSize; cell++) {
            const divCell = document.createElement('div');
            divCell.setAttribute('data-orientation', grid[idx]);
            divRow.appendChild(divCell);
            idx++;
        }
        dest.appendChild(divRow);
        rowSize += 2;
    }
    for (let rowNum = 0; rowNum < rowCount / 2; rowNum++) {
        rowSize -= 2;
        let divRow = document.createElement('div');
        for (let cell = 0; cell < rowSize; cell++) {
            const divCell = document.createElement('div');
            divCell.setAttribute('data-orientation', grid[idx]);
            divRow.appendChild(divCell);
            idx++;
        }
        dest.appendChild(divRow);
    }
}

function processOrangeBlocks(grid, gridSize) {
    // scan for an empty block. It should always be in the top left corner of a 2x2 block of oranges
    let rowNum = 0;
    let rowSize = 2;
    let x = 0;
    for (let idx = 0; idx < grid.length; idx++) {
        if (grid[idx] === '#') {
            // first verify that the other 4 cells are also orange. Figuring out the indexes of those cells is going to be... intertesting
            const tl = idx;
            if (x >= rowSize) {
                throw new Error(`shouldn't be possible to detect an empty cell on the right edge if its the top right of a 2x2 block.`);
            }
            const tr = idx + 1; // the top right is easy, its always just idx +1.
            const extraCell = (rowNum >= gridSize / 2) ? -1 : ((rowNum + 1 == gridSize / 2) ? 0 : 1);
            const bl = idx + rowSize + extraCell; // bottom left will be a whole row of cells later, but we also need to account for the extra cell on the start of the row
            const br = bl + 1; // bottom right is obviously immediately after that. We should really check that this hasn't wrapped onto the next row, but I cba to figure out how to check for that so... I won't
            if (grid[tr] !== '#' || grid[bl] !== '#' || grid[br] !== '#') {
                throw new Error(`orange cell isn't at the top left of a 2x2 grid of orange cells?`)
            };
            const replacement = orangeReplacements[Math.floor(Math.random() * 2)];
            grid[tl] = replacement[0];
            grid[tr] = replacement[1];
            grid[bl] = replacement[2];
            grid[br] = replacement[3];
        }
        x++;

        if (x >= rowSize) {
            x = 0;

            if (rowNum >= gridSize / 2) {
                rowSize -= 2;
            } else if (rowNum + 1 === gridSize / 2) {
                //do nothing
            } else {
                rowSize += 2;
            }

            rowNum++;
        }
    }
}
function expandGrid(grid, gridSize) {
    const newGrid = [];
    let rowSize = 2;
    let oldIdx = 0;
    let idx = 0;
    newGrid.push('#');
    newGrid.push('#');
    for (let rowNum = 0; rowNum < gridSize / 2; rowNum++) {
        newGrid.push('#');
        for (let cell = 0; cell < rowSize; cell++) {
            newGrid.push(grid[idx]);
            idx++;
        }
        newGrid.push('#');
        rowSize += 2;
    }
    for (let rowNum = 0; rowNum < gridSize / 2; rowNum++) {
        rowSize -= 2;
        newGrid.push('#');
        for (let cell = 0; cell < rowSize; cell++) {
            newGrid.push(grid[idx]);
            idx++;
        }
        newGrid.push('#');
    }
    newGrid.push('#');
    newGrid.push('#');
    return newGrid;
}
function removeFacing(grid, gridSize) {
    let rowNum = 0;
    let rowSize = 2;
    let x = 0;
    for (let idx = 0; idx < grid.length; idx++) {
        if (grid[idx] === 'S' || grid[idx] === 's') {
            // check if the block below is N, if yes then remove both
            const extraCell = (rowNum >= gridSize / 2) ? -1 : ((rowNum + 1 == gridSize / 2) ? 0 : 1);

            const bl = idx + rowSize + extraCell;
            if (grid[bl] === 'N' || grid[bl] === 'n') {
                grid[idx] = '#';
                grid[bl] = '#';
            };
        }
        if (grid[idx] === 'E' || grid[idx] === 'e') {
            // check if the block right is W, if yes then remove both
            const bl = idx + 1;
            if (grid[bl] === 'W' || grid[bl] === 'w') {
                grid[idx] = '#';
                grid[bl] = '#';
            };
        }
        x++;
        if (x >= rowSize) {
            x = 0;
            if (rowNum >= gridSize / 2) {
                rowSize -= 2;
            } else if (rowNum + 1 === gridSize / 2) {
                //do nothing
            } else {
                rowSize += 2;
            }
            rowNum++;
        }
    }

}
function shuffle(grid, gridSize) {
    const newGrid = grid.map(x => '#');
    let rowNum = 0;
    let rowSize = 2;
    let x = 0;
    for (let idx = 0; idx < grid.length; idx++) {
        switch (grid[idx]) {
            case '#':
                break;
            case 'N':
            case 'n':
                {
                    const extraCell = (rowNum < gridSize / 2) ? -1 : ((rowNum == gridSize / 2) ? 0 : 1);
                    const target = idx - (rowSize + extraCell); // cell above will be a whole row of cells earlier, but we also need to account for the extra cell on the start of the row
                    newGrid[target] = grid[idx];
                    break;
                }
            case 'S':
            case 's':
                {
                    const extraCell = (rowNum >= gridSize / 2) ? -1 : ((rowNum + 1 == gridSize / 2) ? 0 : 1);
                    const target = idx + rowSize + extraCell; // bottom left will be a whole row of cells later, but we also need to account for the extra cell on the start of the row
                    newGrid[target] = grid[idx];
                    break;
                }
            case 'E':
            case 'e':
                newGrid[idx + 1] = grid[idx];
                break;
            case 'W':
            case 'w':
                newGrid[idx - 1] = grid[idx];
                break;
        }
        x++;
        if (x >= rowSize) {
            x = 0;
            if (rowNum >= gridSize / 2) {
                rowSize -= 2;
            } else if (rowNum + 1 === gridSize / 2) {
                //do nothing
            } else {
                rowSize += 2;
            }
            rowNum++;
        }
    }
    return newGrid;
}

let currentAnimation;

async function runSeq(delay, generator) {
    const enumerator = generator();
    return new Promise((r, x) => {
        function next() {
            if (enumerator.next().done) {
                r();
                return;
            };
            currentAnimation=setTimeout(next, delay);
        }
        next();
    });
}

const btnQuickGenerate = document.getElementById('btnQuickGenerate');
btnQuickGenerate.addEventListener('click', async e => {
    clearTimeout(currentAnimation);
    grid = [...'####'];
    gridSize = 2;
    processOrangeBlocks(grid, gridSize);
    const numIterations = document.getElementById('txtQuickSize').value;
    const divProgress = document.querySelector('#divProgress>div');
    for (let iterations = 0; iterations < numIterations; iterations++) {
        quickExpand();
        divProgress.style.width = (100*iterations / numIterations) + '%';
        await delay(0);
    }
    divProgress.style.width = '100%';
    drawGrid(gridSize, grid, divOutput);
});
const btnQuickAnimate = document.getElementById('btnQuickAnimate');
btnQuickAnimate.addEventListener('click', async e => {
    clearTimeout(currentAnimation);
    grid = [...'####'];
    gridSize = 2;
    processOrangeBlocks(grid, gridSize);
    const numIterations = document.getElementById('txtQuickSize').value;
    for (let iterations = 0; iterations < numIterations; iterations++) {
        await runSeq(getAnimationSpeed(), expandSteps);
    }
    drawGrid(gridSize, grid, divOutput);
});



const btnExpand = document.getElementById('btnExpand');
btnExpand.addEventListener('click', async e => {
    clearTimeout(currentAnimation);
    await runSeq(getAnimationSpeed(), expandSteps);
    drawGrid(gridSize, grid, divOutput);
});


const btnStep = document.getElementById('btnStep');
btnStep.addEventListener('click', async e => {
    clearTimeout(currentAnimation);
    switch (btnStep.getAttribute('data-NextStep')) {
        case '0':
            grid = expandGrid(grid, gridSize);
            gridSize += 2;
            break;
        case '1':
            removeFacing(grid, gridSize);
            break;
        case '2':
            grid = shuffle(grid, gridSize);
            break;
        case '3':
            processOrangeBlocks(grid, gridSize);
            break;
    }
    btnStep.setAttribute('data-NextStep', ( Number(btnStep.getAttribute('data-NextStep'))+1) % 4);
    if (btnStep.getAttribute('data-NextStep')==0) {
        btnExpand.disabled = false;
        btnQuickAnimate.disabled = false;
        btnQuickGenerate.disabled = false;
    } else {
        btnExpand.disabled = true;
        btnQuickAnimate.disabled = true;
        btnQuickGenerate.disabled = true;

    }
    drawGrid(gridSize, grid, divOutput);
});


function getAnimationSpeed() {
    return 10 ** (1000 / document.getElementById('animationSpeed').value);
}
document.getElementById('animationSpeed').addEventListener('change', e=> {
    document.getElementById('lblSpeed').innerText = ((1000/getAnimationSpeed())).toPrecision(3).replace(/(?<=\.[0-9]*)0*$/, '') + 'fps';
});
document.getElementById('animationSpeed').dispatchEvent(new Event('change'));

async function delay(ms) {
    return new Promise((r,x)=> {
        setTimeout(r, ms);
    })
}

document.getElementById('zoom').addEventListener('change', e=> {
    const zoomLevel = (Number(document.getElementById('zoom').value)).toPrecision(3).replace(/(?<=\.[0-9]*)0*$/, '').replace(/\.$/, '');
    divOutput.style.transform = `scale(${zoomLevel/100}`;
    document.getElementById('lblZoom').innerText = `${zoomLevel}%`;
});
document.getElementById('zoom').dispatchEvent(new Event('change'));
